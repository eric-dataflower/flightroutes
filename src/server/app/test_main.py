"""
Performance testing.
"""
import json
import sys
import cProfile
import io
import pstats
from fastapi.encoders import jsonable_encoder

from pstats import SortKey

from app.gateways import to_flight
from app.services import FlightRouterService
from app.routers.services import airports_connection, routes_service


print("Initializing.")
sys.stdout.flush()
from app.routers import neighbors  # pylint: disable=W0611

neighbors.neighbors_and_stops((-1, ""))

router_service = FlightRouterService(airports_connection())
routes_service()
print("Initialized.")
print()


def test_main() -> None:
    """Run list_routes and show performance statistics."""
    source_id = 3318
    destination_id = 465
    max_number_of_stops = 6

    profile = cProfile.Profile()
    profile.enable()
    flights = [
        to_flight(routes)
        for routes in router_service.list_routes(
            source_id, destination_id, max_number_of_stops
        )
    ]
    profile.disable()
    textio = io.StringIO()
    statistics = pstats.Stats(profile, stream=textio).sort_stats(
        SortKey.CUMULATIVE
    )
    statistics.print_stats()
    print(textio.getvalue())

    print(len(flights))
    # for flight in flights:
    #     print(json.dumps(jsonable_encoder(flight.dict()), indent=2))
