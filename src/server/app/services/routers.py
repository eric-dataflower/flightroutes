"""Flight route service."""
from __future__ import annotations
import dataclasses
from typing import List, Optional, Iterable
from app.connections import OpenFlightsAirportsConnection, OpenFlightsAirport
from app.routers.routers import list_routes
from app.routers.identifiers import normalized
from app.gateways import Route


@dataclasses.dataclass
class FlightRouterService:
    """Entry point into library."""

    airports_connection: OpenFlightsAirportsConnection

    def list_airports(
        self, country: Optional[str] = None
    ) -> List[OpenFlightsAirport]:
        """Given a country code, return a list of airports in that country."""
        return self.airports_connection.list_airports(country=country)

    def list_countries(self) -> List[str]:
        """Return a list of all countries where there are airports."""
        return self.airports_connection.list_countries()

    @classmethod
    def list_routes(
        cls,
        source_id: int,
        destination_id: int,
        max_number_of_stops: Optional[int] = None,
    ) -> Iterable[List[Route]]:
        """Return a list of routes for the given airports and constraints."""

        return list_routes(
            normalized(source_id, None),
            normalized(destination_id, None),
            max_number_of_stops,
        )
