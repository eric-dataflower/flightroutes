import React, { useState, useEffect } from 'react';

import {
  IconButton,
  Grid,
  TextField,
  makeStyles,
  CircularProgress,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Backdrop,
} from '@material-ui/core';
import {
  Autocomplete
} from '@material-ui/lab';
import SwapHorizIcon from '@material-ui/icons/SwapHoriz';
import Axios from 'axios';

import { makeUseAxios } from 'axios-hooks'
import './App.css';
import FlightsTable from './FlightsTable';
import FlightsMap from './FlightsMap';
import ReactTooltip from "react-tooltip";


interface FlightParameters {
  fromCountry: string;
  toCountry: string;
  fromAirport: any;
  toAirport: any;
  maxStops: number;
}

const axios = Axios.create({ baseURL: process.env.REACT_APP_API_HOST })
const useAxios = makeUseAxios({
  axios: axios
})

const useStyles = makeStyles((theme) => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(1280 + theme.spacing(2) * 2)]: {
      width: 1280,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  grid: {
    marginTop: 'auto',
    paddingTop: theme.spacing(2),
    //minHeight: "100vh"
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const useCountries = () => {
  return useAxios('/countries/')
}

const useAirports = (country: string) => {
  return useAxios(`/countries/${country}/airports/`)
}


function App() {
  const classes = useStyles();

  const [{ data: countryOptions, loading: countriesLoading, error: countriesError }] = useCountries()
  const [tooltip, setTooltip] = useState("")
  const [flightParameters, setFlightParameters] = useState<FlightParameters>({
    fromCountry: "Australia",
    toCountry: "United Kingdom",
    fromAirport: null,
    toAirport: null,
    maxStops: 6
  })

  const fromCountry = flightParameters.fromCountry;
  const toCountry = flightParameters.toCountry;
  const maxStops = flightParameters.maxStops;

  const [{
    data: fromAirports,
    // loading: fromAirportsLoading,
    error: fromAirportsError }] = useAirports(fromCountry)

  if (fromAirportsError) {
    console.error(fromAirportsError)
  }

  const [{
    data: toAirports,
    // loading: toAirportsLoading,
    error: toAirportsError }] = useAirports(toCountry)

  if (toAirportsError) {
    console.error(toAirportsError)
  }

  const fromAirport = flightParameters.fromAirport || (fromAirports ? fromAirports[0] : null);
  const toAirport = flightParameters.toAirport || (toAirports ? toAirports[0] : null);

  const [flights, setFlights] = useState(null)

  useEffect(() => {
    if (fromAirport !== null && toAirport !== null) {
      setFlights(null)
      axios.get(`/airports/${fromAirport.id}/flights/${toAirport.id}/`, {
        params: {
          max_number_of_stops: maxStops.toString()
        }
      }).then((response) => {
        setFlights(response.data)
      }).catch((error) => {
        console.error(error)
      })
    }
  }, [fromAirport, toAirport, maxStops]);


  const onClick = () => {
    setFlightParameters(Object.assign({}, flightParameters, {
      fromCountry: toCountry,
      toCountry: fromCountry,
      fromAirport: toAirport,
      toAirport: fromAirport,
    }))
  }

  return (
    <div data-tip="" className="App">
      <ReactTooltip>{tooltip}</ReactTooltip>
      <main className={classes.layout}>
        <Grid container spacing={1}
          className={classes.grid}
          alignItems='center'
          justify="center"
          direction="row">
          <Grid item xs={1}>
            <IconButton color="primary" size="small" onClick={onClick}>
              <SwapHorizIcon />
            </IconButton>
          </Grid>
          <Grid item xs={11}>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <Autocomplete
                  options={countryOptions || [fromCountry]}
                  getOptionLabel={country => country}
                  value={fromCountry}
                  onChange={(event: React.ChangeEvent<{}>, value: string | null) => {
                    if (value !== null) {
                      setFlightParameters(Object.assign({}, flightParameters, {
                        fromCountry: value,
                        fromAirports: null,
                        fromAirport: null
                      }));
                    }
                  }}
                  renderInput={params => <TextField {...params}
                    label="From Country" variant="outlined" />}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Autocomplete
                  options={countryOptions || [toCountry]}
                  getOptionLabel={country => country}
                  value={toCountry}
                  onChange={(event: React.ChangeEvent<{}>, value: string | null) => {
                    if (value !== null) {
                      setFlightParameters(Object.assign({}, flightParameters, {
                        toCountry: value,
                        toAirports: null,
                        toAirport: null
                      }));
                    }
                  }}
                  renderInput={params => <TextField {...params}
                    label="To Country" variant="outlined" />}
                />
              </Grid>
            </Grid>
            <Grid container spacing={3}
              alignItems='center'
              justify="center"
              direction="row">
              <Grid item xs={12} sm={6} style={{ textAlign: 'center' }}>
                <Autocomplete
                  options={fromAirports || []}
                  getOptionLabel={airport => (airport?.iata) ?
                    (`(${airport?.iata}) ${airport?.name}`)
                    : airport?.name}
                  value={fromAirport}
                  onChange={(event: React.ChangeEvent<{}>, value: any) => {
                    if (value !== null) {
                      setFlightParameters(Object.assign({}, flightParameters, {
                        fromAirport: value
                      }));
                    }
                  }}
                  renderInput={params => <TextField {...params}
                    label="From Airport" variant="outlined" />}
                />
              </Grid>
              <Grid item xs={12} sm={6} style={{ textAlign: 'center' }}>
                <Autocomplete
                  options={toAirports || []}
                  getOptionLabel={airport => (airport?.iata) ?
                    (`(${airport?.iata}) ${airport?.name}`)
                    : airport?.name}
                  value={toAirport}
                  onChange={(event: React.ChangeEvent<{}>, value: any) => {
                    if (value !== null) {
                      setFlightParameters(Object.assign({}, flightParameters, {
                        toAirport: value
                      }));
                    }
                  }}
                  renderInput={params => <TextField {...params}
                    label="To Airport" variant="outlined" />}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container spacing={1}
          className={classes.grid}
          alignItems='center'
          justify="center"
          direction="row">
          <FormControl component="fieldset">
            <FormLabel component="legend">Max Stops</FormLabel>
            <RadioGroup row aria-label="max stops" name="maxStops"
              value={maxStops.toString()}
              onChange={(event: React.ChangeEvent<{}>, value: string) => {
                if (value !== null) {
                  setFlightParameters(Object.assign({}, flightParameters, {
                    maxStops: parseFloat(value)
                  }));
                }
              }}>
              {
                [...Array(7)].map((e, i) =>
                  <FormControlLabel
                    value={i.toString()}
                    key={i}
                    control={<Radio color="primary" />}
                    label={i}
                    labelPlacement="start"
                  />)
              }
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid container spacing={1}>
          <Grid item xs={12} md={6}>
            <FlightsMap flights={flights || []}
              setTooltip={setTooltip} />
          </Grid>
          <Grid item xs={12} md={6}>
            <FlightsTable flights={flights} />
          </Grid>
        </Grid>
        {countriesLoading || countriesError ?
          <Backdrop className={classes.backdrop} open={true}>
            <CircularProgress color="inherit" />
          </Backdrop> : null
        }
      </main>
    </div>
  );
}

export default App;
