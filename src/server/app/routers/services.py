"""Provide instantiated services using flights data."""
from __future__ import annotations
import functools
import os
from app.connections import (
    OpenFlightsRoutesConnection,
    OpenFlightsAirportsConnection,
)

if False:  # pylint: disable=W0125
    from app.services import (  # pylint: disable=W0611
        OpenFlightsRoutesService,
        OpenFlightsAirportsService,
    )

DIRECTORY = os.path.dirname(os.path.realpath(__file__))


@functools.lru_cache()  # type: ignore
def routes_connection() -> OpenFlightsRoutesConnection:
    with open(os.path.join(DIRECTORY, "../data/routes.dat")) as f:
        return OpenFlightsRoutesConnection(f)


@functools.lru_cache()  # type: ignore
def airports_connection() -> OpenFlightsAirportsConnection:
    with open(os.path.join(DIRECTORY, "../data/airports.dat")) as f:
        return OpenFlightsAirportsConnection(f)


@functools.lru_cache()  # type: ignore
def routes_service() -> "OpenFlightsRoutesService":
    from app.services.routes import (  # pylint: disable=W0621
        OpenFlightsRoutesService,
    )

    return OpenFlightsRoutesService(routes_connection(), airports_connection())


@functools.lru_cache()  # type: ignore
def airports_service() -> "OpenFlightsAirportsService":
    from app.services.airports import (  # pylint: disable=W0621
        OpenFlightsAirportsService,
    )

    return OpenFlightsAirportsService(
        airports_connection(), routes_connection()
    )
