"""Implement getting edges and weights for the router."""
from __future__ import annotations
import dataclasses
from typing import Dict, Tuple, Optional, NamedTuple

from app.algorithms import Number
from app.settings import TRANSIT_TIME, FLIGHT_VELOCITY

from .services import routes_service, airports_service
from .identifiers import Identifier, normalized


Journey = Tuple[
    Tuple[Optional[int], Optional[str]], Tuple[Optional[int], Optional[str]]
]


class StopsAndDistance(NamedTuple):
    """A stop and distance ordinal and number class to use in k_shortest.

    We want to be able to prioritise routes with smallest number of stops,
    and shortest distance.
    """

    value: Tuple[int, float]

    def __lt__(self, other: StopsAndDistance) -> bool:
        return self.value < other.value

    def __add__(self, other: StopsAndDistance) -> StopsAndDistance:
        stops, distance = self.value
        stops_1, distance_1 = other.value
        return StopsAndDistance((stops + stops_1, distance + distance_1))


Number.register(StopsAndDistance)


def _neighbors_and_stops(
    source: Identifier
) -> Dict[Journey, Tuple[Identifier, StopsAndDistance]]:
    from .services import routes_service

    neighbors = routes_service().list_neighbors_and_distance(*source)
    return {
        (source, normalized(destination_id, destination_iata)): (
            normalized(destination_id, destination_iata),
            StopsAndDistance((1, distance)),
        )
        for (destination_id, destination_iata, distance) in neighbors
    }


_prepared_neighbors_and_stops: Dict[
    Identifier, Dict[Journey, Tuple[Identifier, StopsAndDistance]]
] = {}


def neighbors_and_stops(
    source: Identifier
) -> Dict[Journey, Tuple[Identifier, StopsAndDistance]]:
    if not _prepared_neighbors_and_stops:
        _initialize()
    if source in _prepared_neighbors_and_stops:
        return _prepared_neighbors_and_stops[source]
    _prepared_neighbors_and_stops[source] = _neighbors_and_stops(source)
    return _prepared_neighbors_and_stops[source]


_prepared_neighbors_and_distance: Dict[
    Identifier, Dict[Journey, Tuple[Identifier, float]]
] = {}


def _neighbors_and_distance(
    source: Identifier
) -> Dict[Journey, Tuple[Identifier, float]]:
    neighbors = routes_service().list_neighbors_and_distance(*source)
    return {
        (source, normalized(destination_id, destination_iata)): (
            normalized(destination_id, destination_iata),
            distance / FLIGHT_VELOCITY
            + TRANSIT_TIME,  # Hours travel time including transit.
        )
        for (destination_id, destination_iata, distance) in neighbors
    }


def neighbors_and_distance(
    source: Identifier
) -> Dict[Journey, Tuple[Identifier, float]]:
    if not _prepared_neighbors_and_distance:
        _initialize()
    if source in _prepared_neighbors_and_distance:
        return _prepared_neighbors_and_distance[source]
    _prepared_neighbors_and_distance[source] = _neighbors_and_distance(source)
    return _prepared_neighbors_and_distance[source]


def _initialize() -> None:
    for airport in airports_service().list_airports():
        _prepared_neighbors_and_stops[
            normalized(airport.id, airport.iata)
        ] = _neighbors_and_stops((airport.id, airport.iata))
        _prepared_neighbors_and_distance[
            normalized(airport.id, airport.iata)
        ] = _neighbors_and_distance((airport.id, airport.iata))
