import React, { Fragment } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import {
  ThemeProvider,
  Theme,
  Tooltip,
  Typography,
  Button
} from '@material-ui/core'
import { createMuiTheme, withStyles } from '@material-ui/core/styles';

import {
  Flight,
} from './interfaces';


const HtmlTooltip = withStyles((theme: Theme) => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 220,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}))(Tooltip);

const theme = createMuiTheme({
  overrides: {
    MuiTableCell: {
      root: {
        padding: '8px 12px',
      },
    },
  },
});

const styles = (theme: Theme) => ({
  root: {
    [theme.breakpoints.down('sm')]: {
      display: "none"
    },
  },
});

const ResponsiveTableCell = withStyles(styles)(TableCell)

const formatMinutes = (n: number) => {
  const hours = n / 60 ^ 0
  const minutes = Math.round(n % 60).toString()
  return ((hours > 0 ? `${hours} hours and ` : '') + `${minutes} minutes`)
}

export default function FlightsTable({ flights }: { flights: Flight[] | null }) {
  return (
    <ThemeProvider theme={theme}>
      <TableContainer component={Paper}>
        <Table aria-label="flights table">
          <TableHead>
            <TableRow>
              <TableCell>
                <HtmlTooltip
                  title={
                    <React.Fragment>
                      <Typography color="inherit">Estimated Travel Time</Typography>
                      <span>Flights are prioritised according to estimated
                      travel time, assuming 1.5 hours per transit.
                      <br/>
                      It may not be the shortest flight, if the shortest has
                      more stops.</span>
                    </React.Fragment>
                  }
                >
                  <Button style={{ textTransform: 'none' }}><b>Estimated Time</b></Button>
                </HtmlTooltip>
              </TableCell>
              <TableCell>Flight time</TableCell>
              <TableCell>Airline</TableCell>
              <TableCell>Depart</TableCell>
              <TableCell>Arrive</TableCell>
              <ResponsiveTableCell>Distance</ResponsiveTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {flights ? flights.map((row, i) => (
              <Fragment key={i}>
                {
                  row.routes.map((route, j) => (
                    <TableRow key={j}>
                      {j === 0 ? <TableCell component="th" scope="row"
                        rowSpan={row.routes.length} >
                        {formatMinutes(row.estimated_travel_time * 60)}
                      </TableCell> : null}
                      <TableCell align="left">{formatMinutes(route.estimated_travel_time * 60)}</TableCell>
                      <TableCell align="left">{route.airline.airline || ''}</TableCell>
                      <TableCell align="left" style={{ fontWeight: j === 0 ? 'bold' : 'normal' }}>
                        {route.source
                          ? `(${route.source_iata}) ${route.source.name}`
                          : (route.source_iata || route.source_id)}
                      </TableCell>
                      <TableCell align="left"
                        style={{ fontWeight: j === row.routes.length - 1 ? 'bold' : 'normal' }}>
                        {route.destination
                          ? `(${route.destination_iata}) ${route.destination.name}`
                          : (route.destination_iata || route.destination_id)}
                      </TableCell>
                      <ResponsiveTableCell align="left">{Math.round(route.distance)}km</ResponsiveTableCell>
                    </TableRow>
                  ))
                }

              </Fragment>
            )) : <TableRow>
                <TableCell colSpan={6} align="center">
                  <CircularProgress variant="indeterminate" color='primary' style={{ opacity: 0.5 }} />
                </TableCell>
              </TableRow>}
          </TableBody>
        </Table>
      </TableContainer>
    </ThemeProvider>
  );
}
