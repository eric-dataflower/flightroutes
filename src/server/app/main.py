"""
Entry point for API server.
"""
import sys
import os
import logging
from typing import List, Optional
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

sys.path.insert(0, "..")

from app.gateways import Airport, Flight, to_flight
from app.routers.services import (
    airports_service,
    routes_service,
    airports_connection,
)
from app.services import FlightRouterService


app = FastAPI()

frontend_domain_name = os.getenv("FRONTEND_DOMAIN_NAME", "localhost")

if os.getenv("FRONTEND_DOMAIN_NAME"):
    logging.info("Initializing.")
    sys.stdout.flush()
    from app.routers import neighbors  # pylint: disable=W0611

    router_service = FlightRouterService(airports_connection())
    neighbors.neighbors_and_stops((-1, ""))
    routes_service()
    logging.info("Initialized.")

origins = [
    "http://localhost:3000",
    "http://localhost:5000",
    f"http://{frontend_domain_name}",
    f"https://{frontend_domain_name}",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/countries/")
async def list_countries() -> List[str]:
    """Return a list of country names."""
    return airports_service().list_countries()


@app.get("/countries/{country_id}/airports/", response_model=List[Airport])
async def list_airports(country_id: str) -> List[Airport]:
    """Return a list of airports. Only show airports with routes"""
    return airports_service().list_airports(country=country_id)


@app.get(
    "/airports/{source_id}/flights/{destination_id}/",
    response_model=List[Flight],
)
async def list_airport_routes(
    source_id: int,
    destination_id: int,
    max_number_of_stops: Optional[int] = None,
):
    """
    Find 2 routes from Airport-A to Airport-B.

    One route is fastest/shortest and another is backup route, it cannot be
    the same with fastest and exceed number of airports configured in UI.
    There is no requirements for best or backup airlines, any available on a
    route can be used.
    """

    return [
        to_flight(routes)
        for routes in router_service.list_routes(
            source_id, destination_id, max_number_of_stops
        )
    ]
