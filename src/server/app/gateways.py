import functools
from decimal import Decimal
from typing import Optional, Tuple, List
from pydantic import BaseModel

from app.settings import TRANSIT_TIME, FLIGHT_VELOCITY
from .connections import OpenFlightsRoute, OpenFlightsAirport
from .routers.identifiers import normalized

Journey = Tuple[
    Tuple[Optional[int], Optional[str]], Tuple[Optional[int], Optional[str]]
]


class Airport(BaseModel):
    """FlightRoute Airport format."""

    id: Optional[int]
    iata: Optional[str]
    name: str
    country: str
    latitude: Decimal
    longitude: Decimal


class Airline(BaseModel):
    id: int
    airline: str


class Route(BaseModel):
    """Describes a flight route."""

    airline: Airline
    source_id: Optional[int]
    source_iata: Optional[str]
    source: Optional[Airport]
    destination_id: Optional[int]
    destination_iata: Optional[str]
    destination: Optional[Airport]
    distance: float
    estimated_travel_time: float
    flight_number: str

    @property
    def journey(self) -> Journey:
        """Return the journey, normalized to allow use in dictionary."""
        return (
            normalized(self.source_id, self.source_iata),
            normalized(self.destination_id, self.destination_iata),
        )


class Flight(BaseModel):
    routes: List[Route]
    estimated_travel_time: float


@functools.lru_cache(maxsize=65536)  # type: ignore
def to_route(route: OpenFlightsRoute) -> Route:
    from .routers.services import airports_service

    source = normalized(route.source_airport_id, route.source_airport)
    source_id, source_iata = source
    destination = normalized(
        route.destination_airport_id, route.destination_airport
    )
    destination_id, destination_iata = destination

    distance = airports_service().get_distance(source_id, destination_id)

    return Route(
        airline=Airline(
            id=route.airline_id or -1, airline=route.airline or ""
        ),
        source_id=source_id,
        source_iata=source_iata,
        source=(
            airports_service().get_by_id(source_id)
            or airports_service().get_by_iata(source_iata)
            or None
        ),
        destination_id=destination_id,
        destination_iata=destination_iata,
        destination=(
            airports_service().get_by_id(destination_id)
            or airports_service().get_by_iata(destination_iata)
            or None
        ),
        distance=distance,
        # estimated_travel_time: 800km per hour.
        estimated_travel_time=distance / FLIGHT_VELOCITY,
        flight_number=route.equipment,
    )


def to_flight(routes: List[Route]) -> Flight:
    """
    Estimate travel time:
    1. 1.5 hours per transit.
    2. 800 km per hour.
    """
    travel_time = (len(routes) - 1) * TRANSIT_TIME + sum(
        route.estimated_travel_time for route in routes
    )

    return Flight(routes=routes, estimated_travel_time=travel_time)


def to_airport(airport: OpenFlightsAirport) -> Airport:
    return Airport(
        id=airport.id,
        iata=airport.iata,
        name=airport.name,
        country=airport.country,
        latitude=airport.latitude,
        longitude=airport.longitude,
    )
