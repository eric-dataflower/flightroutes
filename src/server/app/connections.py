"""Connection to airport and route data sources."""

import csv
from decimal import Decimal
from typing import Optional, List, TextIO, NamedTuple, Dict, Tuple


class OpenFlightsAirport(NamedTuple):
    """See https://openflights.org/data.html on schema of data/airports.dat."""

    id: Optional[int]
    name: str
    city: str
    country: str
    iata: Optional[str]
    icao: Optional[str]
    latitude: Decimal
    longitude: Decimal
    altitude: Decimal
    tz_offset: str
    dst: str
    tz: str
    type: str
    source: str


class OpenFlightsRoute(NamedTuple):
    """See https://openflights.org/data.html on schema of data/routes.dat."""

    airline: str
    airline_id: Optional[int]
    source_airport: str
    source_airport_id: Optional[int]
    destination_airport: str
    destination_airport_id: Optional[int]
    codeshare: bool
    stops: int
    equipment: str


def _to_str(value: str) -> Optional[str]:
    return str(value) if value != "\\N" else None


class OpenFlightsAirportsConnection:
    """Provide an interface into data/airports.dat."""

    def __init__(self, csvfile: TextIO) -> None:  # pylint: disable=R0914
        self.csvfile = csvfile
        reader = csv.reader(csvfile)
        self.airports: List[OpenFlightsAirport] = []
        self.countries: Dict[str, List[OpenFlightsAirport]] = {}
        self.iatas: Dict[str, OpenFlightsAirport] = {}
        self.ids: Dict[int, OpenFlightsAirport] = {}

        for row in reader:
            (
                airport_id,
                name,
                city,
                country,
                iata,
                icao,
                latitude,
                longitude,
                altitude,
                tz_offset,
                dst,
                timezone,
                structure_type,
                source,
            ) = row
            airport = OpenFlightsAirport(
                int(airport_id),
                name,
                city,
                country,
                _to_str(iata),
                _to_str(icao),
                Decimal(latitude),
                Decimal(longitude),
                Decimal(altitude),
                tz_offset,
                dst,
                timezone,
                structure_type,
                source,
            )
            self.airports.append(airport)
            self.countries.setdefault(airport.country, []).append(airport)

            if airport.iata:
                assert airport.iata not in self.iatas
                self.iatas[airport.iata] = airport

            assert airport.id is not None
            assert airport.id not in self.ids
            self.ids[airport.id] = airport

    def list_airports(
        self, country: Optional[str] = None
    ) -> List[OpenFlightsAirport]:
        if country:
            return self.countries[country]

        return self.airports

    def list_countries(self) -> List[str]:
        """Return a list of countries with airports."""
        return sorted(self.countries.keys())

    def get_by_iata(self, iata: str) -> Optional[OpenFlightsAirport]:
        """Return airport with given iata code."""
        return self.iatas.get(iata, None)

    def get_by_id(self, airport_id: int) -> Optional[OpenFlightsAirport]:
        """Return airport with given id."""
        return self.ids.get(airport_id, None)

    def __hash__(self) -> int:
        return hash(self.csvfile)


def _to_int(value: str) -> Optional[int]:
    return int(value) if value != "\\N" else None


class OpenFlightsRoutesConnection:
    """Provide an interface into data/routes.dat."""

    def __init__(self, csvfile: TextIO) -> None:  # pylint: disable=R0914
        self.csvfile = csvfile
        reader = csv.reader(csvfile)
        self.routes: List[OpenFlightsRoute] = []
        self.source_iatas: Dict[str, List[OpenFlightsRoute]] = {}
        self.destination_iatas: Dict[str, List[OpenFlightsRoute]] = {}
        self.source_ids: Dict[int, List[OpenFlightsRoute]] = {}
        self.destination_ids: Dict[int, List[OpenFlightsRoute]] = {}

        for row in reader:
            (
                airline,
                airline_id,
                source_airport,
                source_airport_id,
                destination_airport,
                destination_airport_id,
                codeshare,
                stops,
                equipment,
            ) = row
            route = OpenFlightsRoute(
                airline,
                _to_int(airline_id),
                source_airport,
                _to_int(source_airport_id),
                destination_airport,
                _to_int(destination_airport_id),
                codeshare == "Y",
                int(stops),
                equipment,
            )
            self.routes.append(route)

            if route.source_airport_id:
                self.source_ids.setdefault(route.source_airport_id, []).append(
                    route
                )

            if route.source_airport:
                self.source_iatas.setdefault(route.source_airport, []).append(
                    route
                )

            if route.destination_airport_id:
                self.destination_ids.setdefault(
                    route.destination_airport_id, []
                ).append(route)

            if route.destination_airport:
                self.destination_iatas.setdefault(
                    route.destination_airport, []
                ).append(route)

    def __hash__(self) -> int:
        return hash(self.csvfile)

    def list_routes_from(
        self,
        source_id: Optional[int] = None,
        source_iata: Optional[str] = None,
    ) -> List[OpenFlightsRoute]:
        """Return a list of routes from given airport."""

        assert source_id is not None or source_iata is not None

        routes = []

        if source_id is not None:
            routes = self.source_ids.get(source_id, [])
        elif source_iata is not None:
            routes = self.source_iatas.get(source_iata, [])

        return routes

    def list_routes_to(
        self,
        destination_id: Optional[int] = None,
        destination_iata: Optional[str] = None,
    ) -> List[OpenFlightsRoute]:
        """Return a list of routes to given airport."""

        assert destination_id is not None or destination_iata is not None

        routes = []

        if destination_id is not None:
            routes = self.destination_ids.get(destination_id, [])
        elif destination_iata is not None:
            routes = self.destination_iatas.get(destination_iata, [])

        return routes
