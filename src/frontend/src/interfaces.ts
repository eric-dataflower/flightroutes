
export interface Airline {
  id: string;
  airline: string;
}

export interface Airport {
  id: number | null;
  iata: string | null;
  name: string;
  country: string;
  latitude: number;
  longitude: number;
}

export interface Route {
  airline: Airline;
  source_id: number;
  source_iata: string;
  source: Airport | null;
  destination_id: number;
  destination_iata: string;
  destination: Airport | null;
  distance: number;
  estimated_travel_time: number;
  flight_number: string;
}

export interface Flight {
  estimated_travel_time: number;
  routes: Route[];
}

