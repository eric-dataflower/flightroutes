"""Tests `connections` module."""

from decimal import Decimal
import os
import pytest  # type: ignore
from .connections import (
    OpenFlightsAirportsConnection,
    OpenFlightsRoutesConnection,
    OpenFlightsAirport,
    OpenFlightsRoute,
)

DIRECTORY = os.path.dirname(os.path.realpath(__file__))


@pytest.fixture(scope="module")  # type: ignore
def airports_connection() -> OpenFlightsAirportsConnection:  # type: ignore
    """Return OpenFlightsAirportsConnection initialized with csv file."""
    with open(os.path.join(DIRECTORY, "data/airports.dat")) as csvfile:
        return OpenFlightsAirportsConnection(csvfile)


@pytest.fixture(scope="module")  # type: ignore
def routes_connection() -> OpenFlightsRoutesConnection:  # type: ignore
    """Return OpenFlightsRoutesConnection initialized with csv file."""
    with open(os.path.join(DIRECTORY, "data/routes.dat")) as csvfile:
        return OpenFlightsRoutesConnection(csvfile)


def test_list_countries(
    airports_connection: OpenFlightsAirportsConnection  # pylint: disable=W0621
) -> None:
    """Test `OpenFlightsAirportsConnection.list_countries`."""
    countries = airports_connection.list_countries()
    assert len(countries) > 0
    assert "Australia" in countries


def test_list_airports(
    airports_connection: OpenFlightsAirportsConnection  # pylint: disable=W0621
) -> None:
    """Test `OpenFlightsAirportsConnection.list_airports`."""
    airports = airports_connection.list_airports()
    assert len(airports) > 0

    # Make sure the airport fields are deserialized to correct type.
    assert (
        OpenFlightsAirport(
            3361,
            "Sydney Kingsford Smith International Airport",
            "Sydney",
            "Australia",
            "SYD",
            "YSSY",
            Decimal("-33.94609832763672"),
            Decimal("151.177001953125"),
            Decimal(21),
            "10",
            "O",
            "Australia/Sydney",
            "airport",
            "OurAirports",
        )
        in airports
    )

    # Check id is present in all airports in airports.dat
    for airport in airports:
        assert airport.id is not None


def test_list_airports_countries(
    airports_connection: OpenFlightsAirportsConnection  # pylint: disable=W0621
) -> None:
    """Test `OpenFlightsAirportsConnection.list_airports with country filter.`"""
    airports = airports_connection.list_airports(country="Tuvalu")
    assert airports == [
        OpenFlightsAirport(
            id=4077,
            name="Funafuti International Airport",
            city="Funafuti",
            country="Tuvalu",
            iata="FUN",
            icao="NGFU",
            latitude=Decimal("-8.525"),
            longitude=Decimal("179.195999"),
            altitude=Decimal("9"),
            tz_offset="12",
            dst="U",
            tz="Pacific/Funafuti",
            type="airport",
            source="OurAirports",
        )
    ]


def test_get_by_id(
    airports_connection: OpenFlightsAirportsConnection  # pylint: disable=W0621
) -> None:
    airport = airports_connection.get_by_id(3361)
    assert airport == OpenFlightsAirport(
        id=3361,
        name="Sydney Kingsford Smith International Airport",
        city="Sydney",
        country="Australia",
        iata="SYD",
        icao="YSSY",
        latitude=Decimal("-33.94609832763672"),
        longitude=Decimal("151.177001953125"),
        altitude=Decimal("21"),
        tz_offset="10",
        dst="O",
        tz="Australia/Sydney",
        type="airport",
        source="OurAirports",
    )


def test_get_by_iata(
    airports_connection: OpenFlightsAirportsConnection  # pylint: disable=W0621
) -> None:
    airport = airports_connection.get_by_iata("SYD")
    assert airport == OpenFlightsAirport(
        id=3361,
        name="Sydney Kingsford Smith International Airport",
        city="Sydney",
        country="Australia",
        iata="SYD",
        icao="YSSY",
        latitude=Decimal("-33.94609832763672"),
        longitude=Decimal("151.177001953125"),
        altitude=Decimal("21"),
        tz_offset="10",
        dst="O",
        tz="Australia/Sydney",
        type="airport",
        source="OurAirports",
    )


def test_list_routes_from_airport(
    airports_connection: OpenFlightsAirportsConnection,  # pylint: disable=W0621
    routes_connection: OpenFlightsRoutesConnection,  # pylint: disable=W0621
) -> None:
    airport = airports_connection.get_by_iata("FUN")
    assert airport
    routes = routes_connection.list_routes_from(source_id=airport.id)

    for route in routes:
        assert route.source_airport == "FUN"

    assert routes == [
        OpenFlightsRoute(
            airline="FJ",
            airline_id=879,
            source_airport="FUN",
            source_airport_id=4077,
            destination_airport="SUV",
            destination_airport_id=1961,
            codeshare=False,
            stops=0,
            equipment="AT5",
        )
    ]

    routes = routes_connection.list_routes_to(destination_id=airport.id)
    assert len(routes) > 0
    for route in routes:
        assert route.destination_airport == "FUN"


def test_list_routes_from_bad_airport(
    routes_connection: OpenFlightsRoutesConnection  # pylint: disable=W0621
) -> None:
    routes = routes_connection.list_routes_from(source_iata="bad_airport")
    assert len(routes) == 0

    routes = routes_connection.list_routes_to(destination_iata="bad_airport")
    assert len(routes) == 0
