"""Implementation of K-Shortest Algorithm.

It can be used with any type of node, and any type of distance measurement
that implements `__add__` and `__lt__`.

See https://gist.github.com/kachayev/5990802#gistcomment-3090159.
See https://en.wikipedia.org/wiki/K_shortest_path_routing#Algorithm.
"""
from __future__ import annotations
import sys
from abc import abstractmethod
import heapq
from collections import defaultdict
from typing import (
    TypeVar,
    List,
    Tuple,
    Mapping,
    DefaultDict,
    Callable,
    Any,
    Optional,
)

if sys.version_info >= (3, 8):
    from typing import Protocol  # pylint: disable=no-name-in-module
else:
    from typing_extensions import Protocol

Node = TypeVar("Node")
Edge = TypeVar("Edge")


class Number(Protocol):
    """
    Implementing our own Abstract Base Class with numeric operations we need:
    See https://github.com/python/mypy/issues/2636#issuecomment-579870113.
    """

    @abstractmethod
    def __add__(self, other: Any) -> Number:  # type: ignore
        raise NotImplementedError

    @abstractmethod
    def __lt__(self, other: Any) -> bool:  # type: ignore
        raise NotImplementedError


def k_shortest(
    start: Node,
    stop: Node,
    edges: Callable[[Node], Mapping[Edge, Tuple[Node, Number]]],
    k: int = 2,
) -> List[List[Edge]]:
    """Return a list of K-shortest paths.

    Implementation of K-Shortest Algorithm.
    """
    paths: List[List[Edge]] = []
    counts: DefaultDict[Node, int] = defaultdict(lambda: 0)
    remaining: List[Tuple[Optional[Number], Node, List[Edge]]] = [
        (None, start, [])
    ]

    while remaining and counts[stop] < k:
        cost, node, path = heapq.heappop(remaining)
        counts[node] += 1
        if node == stop:
            paths.append(path)

        if counts[node] <= k:
            for adjacency, (neighbor, weight) in edges(node).items():
                heapq.heappush(
                    remaining,
                    (
                        cost + weight if cost is not None else weight,
                        neighbor,
                        [adjacency] + path,
                    ),
                )
    return paths
