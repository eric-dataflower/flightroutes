"""Tests `services` module."""
from decimal import Decimal
import os
import pytest  # type: ignore
from .connections import OpenFlightsAirportsConnection
from .services import FlightRouterService

DIRECTORY = os.path.dirname(os.path.realpath(__file__))


@pytest.fixture(scope="module")  # type: ignore
def airports_connection() -> OpenFlightsAirportsConnection:  # type: ignore
    """Return OpenFlightsAirportsConnection initialized with csv file."""
    with open(os.path.join(DIRECTORY, "data/airports.dat")) as csvfile:
        return OpenFlightsAirportsConnection(csvfile)


def test_list_routes(
    airports_connection: OpenFlightsAirportsConnection,  # pylint: disable=W0621
) -> None:
    source = airports_connection.get_by_iata("SKT")
    destination = airports_connection.get_by_iata("FUN")

    service = FlightRouterService(airports_connection)

    assert source
    assert destination
    assert len(list(service.list_routes(source.id, destination.id))) == 2

    source = airports_connection.get_by_iata("AGV")
    destination = airports_connection.get_by_iata("SYD")

    assert source
    assert destination
    assert len(list(service.list_routes(source.id, destination.id))) == 0
