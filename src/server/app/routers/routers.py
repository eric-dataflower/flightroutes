"""Domain logic for `flightroutes`.

Find 2 routes from Country-Airport-A to Country-Airport-B.

One route is fastest/shortest and another is backup route, it cannot be the
same with fastest and exceed number of airports configured in UI. There is no
requirements for best or backup airlines, any available on a route can be used.
"""
from __future__ import annotations
from typing import List, Optional
from app.algorithms import k_shortest

from .neighbors import Journey, neighbors_and_distance, neighbors_and_stops
from .identifiers import Identifier, normalized
from .services import routes_service

if False:
    from app.gateways import Route


def list_paths(
    source: Identifier,
    destination: Identifier,
    max_number_of_stops: Optional[int] = None,
) -> List[List[Journey]]:
    """
    Return the fastest route, and a backup route that do not exceed
    max_number_of_stops if it is specified.
    """
    # Shortest route.
    shortest_path = None
    least_stops_path = None

    shortest_paths = k_shortest(
        normalized(*source),
        normalized(*destination),
        neighbors_and_distance,
        k=1,
    )

    if shortest_paths:
        shortest_path = shortest_paths[0]

        # Least stops
        least_stops_paths = k_shortest(
            normalized(*source),
            normalized(*destination),
            neighbors_and_stops,
            k=2,
        )
        for path in least_stops_paths:
            if tuple(shortest_path) != tuple(path) and (
                max_number_of_stops is None
                or (len(path) - 2)
                <= max_number_of_stops  # Exclude start, finish
            ):
                least_stops_path = path
                break

    return list(
        filter(
            None,
            [
                list(reversed(shortest_path or [])),
                list(reversed(least_stops_path or [])),
            ],
        )
    )


def list_routes(
    source: Identifier,
    destination: Identifier,
    max_number_of_stops: Optional[int] = None,
) -> List[List["Route"]]:
    journey_paths = list_paths(source, destination, max_number_of_stops)
    route_paths = []
    for path in journey_paths:
        routes = []
        for journey in path:
            _source, _destination = journey
            source_id, source_iata = _source
            destination_id, destination_iata = _destination
            route = routes_service().list_routes(
                source_id, source_iata, destination_id, destination_iata
            )[0]
            routes.append(route)
        route_paths.append(routes)
    return route_paths
