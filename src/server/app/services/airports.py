"""Flight route service."""
from __future__ import annotations
import functools
import dataclasses
import math
from typing import List, Optional
from app.connections import (
    OpenFlightsAirportsConnection,
    OpenFlightsRoutesConnection,
)
from app.gateways import Airport, to_airport


@dataclasses.dataclass
class OpenFlightsAirportsService:
    airports_connection: OpenFlightsAirportsConnection
    routes_connection: OpenFlightsRoutesConnection

    def __hash__(self) -> int:
        return hash(self.airports_connection)

    def has_routes(self, id, iata) -> bool:
        """Return whether the airport has a route arriving or leaving it."""
        return bool(
            self.routes_connection.list_routes_from(
                source_id=id, source_iata=iata
            )
            or self.routes_connection.list_routes_to(
                destination_id=id, destination_iata=iata
            )
        )

    @functools.lru_cache(maxsize=65536)  # type: ignore
    def list_airports(self, country: Optional[str] = None) -> List[Airport]:
        """Given a country code, return airports with routes in the country."""
        return [
            to_airport(airport)
            for airport in self.airports_connection.list_airports(
                country=country
            )
            if self.has_routes(airport.id, airport.iata)
        ]

    @functools.lru_cache()  # type: ignore
    def list_countries(self) -> List[str]:
        """Return a list of countries where there are airports with routes."""
        return [
            country
            for country in self.airports_connection.list_countries()
            if self.list_airports(country)
        ]

    def get_by_id(self, airport_id: int) -> Optional[Airport]:
        airport = self.airports_connection.get_by_id(airport_id)
        return to_airport(airport) if airport else None

    def get_by_iata(self, iata: str) -> Optional[Airport]:
        airport = self.airports_connection.get_by_iata(iata)
        return to_airport(airport) if airport else None

    def get_distance(self, source_id: int, destination_id: int) -> float:
        """
        Return distance between two airports, in kilometres.

        Using Haversine.

        See https://kite.com/python/answers/how-to-find-the-distance-between-two-lat-long-coordinates-in-python
        """
        R = 6373.0

        source = self.get_by_id(source_id)
        destination = self.get_by_id(destination_id)

        if source and destination:
            lat1, lon1 = (
                math.radians(source.latitude),
                math.radians(source.longitude),
            )
            lat2, lon2 = (
                math.radians(destination.latitude),
                math.radians(destination.longitude),
            )

            dlon = lon2 - lon1
            dlat = lat2 - lat1

            a = (
                math.sin(dlat / 2) ** 2
                + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2
            )
            c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
            distance = R * c
            return distance

        return float("inf")
