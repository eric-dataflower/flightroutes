# Flight Routes #

- [flightroutes-int.dataflower.com.au](http://flightroutes-int.dataflower.com.au)
- [API](http://flightroutes-api.int.dataflower.com.au)

## About ##

You company is a tour operator and sell flight tickets around the world.
Management asked you to make an application to let customers find 2 routes
from Country-Airport-A to Country-Airport-B. One route is fastest/shortest and
another is backup route, it cannot be the same with fastest and exceed number
of airports configured in UI. There is no requirements for best or backup
airlines, any available on a route can be used. User need to select country and
airport available in select country, and input number hops for backup flight.
Result should be displayed on a map with airport names, Airline, Flight number,
route path, distance and estimated travel time.

![Example Image](screenshot.png "Find flights")

## Requirements ##

- [FastAPI backend](src/server/app/main.py)
- [React frontend](src/frontend)
- [README.md](README.md)
- [Dockerfile](src/server/Dockerfile)
- Supports 50 simultaneous users (See later.)

## What's interesting? ##

### Backend Programming ###
- [Domain logic](src/server/app/routers/routers.py#L34)
- [K-shortest routing](src/server/app/algorithms.py#L49)
- [Travel time estimate](src/server/app/gateways.py#L103)
- [Pydantic models](src/server/app/gateways.py#L15)

### Performance & Testing ###
- [Docker Compose](src/server/docker-compose.yml)
- [Python 3 Performance Profiling](src/server/app/test_main.py#L36)
- [ab Performance Test](src/server/Makefile#L30)
- [Basic unit tests](src/server/app/test_connections.py)

### Deployment & Operations ###
- [CI/CD](.gitlab-ci.yml)
- [Terraform deployment](main.tf)
- [AWS networking](https://gitlab.com/eric-dataflower/infra/-/blob/master/src/vpc/modules/dev/main.tf)
- [ECS hosted Docker](https://gitlab.com/eric-dataflower/infra/-/blob/master/src/ecs/main.tf)
- [CORS configured dynamically during CI/CD](src/server/app/main.py#L25)

### Frontend Programming ###
- [Material UI Form](src/frontend/src/App.tsx)
- [React-Simple-Map](src/frontend/src/FlightsMap.tsx)
- [Material UI Table](src/frontend/src/FlightsTable.tsx)

## Journey to 50 simultaneous users ##

The first iteration of the project took up to 4 seconds to calculate a route.

As stop gap, I cached the route calculations.

This worked when repeating the same search, but 50 users behave in different
ways. I do want to get to 50 simultaneous users.

1. I took off the cache from the route search.
2. I experimented with using [Docker Compose](src/server/docker-compose.yml)'s
  [--scale](src/server/Makefile#L3) option, to add more workers on my local
  machine, and profiled performance using - [ab](src/server/Makefile#L30). As
  the project is CPU bound by the route calculation, this offered no
  improvements to throughput on my laptop.
3. So I went to looking more closely at the code, profiling the route
calculation logic using
[cProfile](src/server/app/test_main.py#L36).
  1. Besides the main `k_shortest` algorithm, it turned out a lot of time was
    spent in `neighbors_and_distance` and `neighbors_and_stops` functions.
    They looked up the edges and weights for each Airport. Since the list of
    airports was available at launch, I could pre-compute the edges and
    weights, which I implemented.
  2. With a couple of other changes, I reduced the cost to calculating a new
    route from up to 4 seconds to less than 1 second. More aggressive
    pre-computation of functions like `get_distance`, called over 500 times
    and costs 0.1 seconds to get distance between two airports with a route
    between them, can have further yields.
4. If one ECS instance can support 2 requests per second, then I can scale to
  50 simultaneous users with perhaps 5 instances, if they don't all search at
  the same point of time.

  To support 50 requests in one second, 25 instances are required, less if we
  let the users wait a few seconds, and if they wait a bit between searches.

At this point it's getting expensive, can it be done better?!

## What can be next? ##

- Separate out frontend and backend deployment & CI/CD.
- Better documentation.
- Consider refactoring frontend page into components.
- Follow through with the plan to deploy a production environment,
  make sure the process & plan is actually good.

## How to start? ##

### Dependencies ###

- Docker
- Terraform
- Python
- GNU Make

### Run locally ###

The local server binds to port 80.

1. To run in development mode, and start container locally:
  - In [src/server](src/server):
    1. `make run`
    2. `make logs` to watch the server logs.
    3. Go to `http://localhost`
  - In [src/frontend](src/frontend):
    1. `npm install`
    2. `npm start`

### Run tests ###

1. `pipenv install`
2. `pipenv shell`

- In [src/server](src/server):
  1. Run unit tests using `make test`
  2. View Python 3 profiling results using `make profile`

### Run performance tests ###

- In [src/server](src/server):
  1. Run docker-compose using. `make up`.
  2. Run `ab` using `make ab`.
  3. Shutdown docker containers using `make down`.
