from __future__ import annotations
import functools
from typing import Tuple, Optional, NamedTuple


if False:  # pylint: disable=W0125
    from .services import (  # pylint: disable=W0611
        OpenFlightsRoutesService,
        OpenFlightsAirportsService,
    )

Identifier = Tuple[int, str]


@functools.lru_cache(maxsize=65536)  # type: ignore
def normalized(
    airport_id: Optional[int], airport_iata: Optional[str]
) -> Tuple[int, str]:
    """Normalize an ID/IATA pair by looking up airports database."""
    from .services import airports_service

    if airport_id and airport_iata:
        return (airport_id, airport_iata)

    if airport_id is None and airport_iata is not None:
        airport = airports_service().get_by_iata(airport_iata)
        if airport:
            return (airport.id or -1, airport.iata or "")
    elif airport_iata is None and airport_id is not None:
        airport = airports_service().get_by_id(airport_id)
        if airport:
            return (airport.id or -1, airport.iata or "")
    return (airport_id or -1, airport_iata or "")
