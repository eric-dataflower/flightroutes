# Apply this terraform to run the container locally.

provider "docker" {
  host = "unix:///var/run/docker.sock"

  registry_auth {
    address = "registry.gitlab.com"
  }
}

data "docker_registry_image" "server" {
  name = "registry.gitlab.com/eric-dataflower/flightroutes/server:latest"
}

resource "docker_image" "server" {
  name = data.docker_registry_image.server.name
  pull_triggers =[data.docker_registry_image.server.sha256_digest]
  keep_locally = true
}

resource "docker_container" "server" {
  image = docker_image.server.latest
  name  = "flightroutes-server"
  env = toset(["GUNICORN_CMD_ARGS=--reload", "FRONTEND_DOMAIN_NAME=localhost"])
  command = ["/start-reload.sh"]

  ports {
    internal = 80
    external = 80
  }
  volumes {
    container_path = "/app"
    read_only = true
    host_path = abspath("./app")
  }
}

output "id" {
  value = docker_container.server.id
}
