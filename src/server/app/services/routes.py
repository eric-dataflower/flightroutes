"""Flight route service."""
from __future__ import annotations
import functools
import dataclasses
from typing import Optional, Dict, Tuple, List
from app.connections import (
    OpenFlightsRoutesConnection,
    OpenFlightsAirportsConnection,
)
from app.gateways import Route


@dataclasses.dataclass
class OpenFlightsRoutesService:
    routes_connection: OpenFlightsRoutesConnection
    airports_connection: OpenFlightsAirportsConnection

    def __init__(
        self,
        routes_connection: OpenFlightsRoutesConnection,
        airports_connection: OpenFlightsAirportsConnection,
    ):
        self.routes_connection = routes_connection
        self.airports_connection = airports_connection
        self._neighbors_and_distance: Dict[
            Tuple[Optional[int], Optional[str]],
            List[Tuple[Optional[int], Optional[str], float]],
        ] = {}

        for airport in airports_connection.list_airports():
            self._neighbors_and_distance[
                airport.id, airport.iata
            ] = self._list_neighbors_and_distance(airport.id, airport.iata)

    def __hash__(self) -> int:
        return hash(self.routes_connection)

    @functools.lru_cache(maxsize=65536)  # type: ignore
    def list_routes(
        self,
        source_id: Optional[int],
        source_iata: Optional[str],
        destination_id: Optional[int],
        destination_iata: Optional[str],
    ) -> List[Route]:
        """Return a list of routes with given source and/or destination."""
        from app.gateways import to_route

        routes = self.routes_connection.list_routes_from(
            source_id=source_id, source_iata=source_iata
        )
        if destination_id is None and destination_iata is None:
            return list(map(to_route, routes))

        return [
            to_route(route)
            for route in routes
            if route.destination_airport_id == destination_id
            or route.destination_airport == destination_iata
        ]

    def _list_neighbors_and_distance(
        self, source_id: Optional[int], source_iata: Optional[str]
    ) -> List[Tuple[Optional[int], Optional[str], float]]:
        """``
        Return list of routes, and distance from an airport.
        """
        from app.gateways import to_route
        from app.routers.services import airports_service

        return [
            (
                route.destination_airport_id,
                route.destination_airport,
                (
                    airports_service().get_distance(
                        source_id, route.destination_airport_id
                    )
                    if source_id is not None
                    and route.destination_airport_id is not None
                    else float("inf")
                ),
            )
            for route in self.routes_connection.list_routes_from(
                source_id=source_id, source_iata=source_iata
            )
        ]

    def list_neighbors_and_distance(
        self, source_id: Optional[int], source_iata: Optional[str]
    ) -> List[Tuple[Optional[int], Optional[str], float]]:
        if (source_id, source_iata) not in self._neighbors_and_distance:
            self._neighbors_and_distance[
                (source_id, source_iata)
            ] = self._list_neighbors_and_distance(source_id, source_iata)

        return self._neighbors_and_distance[(source_id, source_iata)]
