import React from "react";
import {
  ComposableMap, Geographies,
  Geography, ZoomableGroup, Graticule, Line, Marker, Point
} from "react-simple-maps";
import red from '@material-ui/core/colors/red';
import blue from '@material-ui/core/colors/blue';
import lime from '@material-ui/core/colors/lime';
import lightGreen from '@material-ui/core/colors/lightGreen';
import {
  Flight,
} from './interfaces';
import { Paper } from "@material-ui/core";
const geoUrl =
  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const routeColors = [
  blue[500],
  red[500]
]

const formatMinutes = (n: number) => {
  const hours = n / 60 ^ 0
  const minutes = Math.round(n % 60).toString()
  return ((hours > 0 ? `${hours} hours and ` : '') + `${minutes} minutes`)
}

export default ({ flights, setTooltip }: { flights: Flight[], setTooltip: CallableFunction }) => {
  const airports: any = {}
  const countries: any = {}

  flights.forEach((flight) => {
    flight.routes.forEach((route) => {
      const source = (route.source
        || { iata: route.source_iata, id: route.source_id, country: null })
      const destination = (route.destination
        || { iata: route.destination_iata, id: route.destination_id, country: null })
      if (source.country) {
        countries[source.country] = true
      }
      if (destination.country) {
        countries[destination.country] = true
      }
      if (route.source_id) {
        airports[route.source_id.toString()] = source
      }
      else if (route.source_iata) {
        airports[route.source_iata] = source
      }
      if (route.source_id) {
        airports[route.destination_id.toString()] = destination
      }
      else if (route.source_iata) {
        airports[route.destination_iata] = destination
      }
    })
  })
  // Result should be displayed on a map with airpot names, Airline, Flight number, route path, distance and estimated travel time.

  return (
    <Paper>
      <ComposableMap projection="geoConicConformal"
        projectionConfig={
          { scale: 150, rotate: [0, -15, 0] }
        }>
        <ZoomableGroup zoom={1}>
          <Graticule stroke="#DDD" />
          <Geographies geography={geoUrl}>
            {({ geographies }) =>
              geographies.map(geo => {
                return <Geography key={geo.rsmKey} geography={geo}

                  style={{
                    default: {
                      fill: countries[geo.properties.NAME] ? lime[700] : lightGreen[900],
                    },
                    hover: {
                      fill: lime[200],
                    }
                  }} />
              })
            }

          </Geographies>
          {flights.slice(0).reverse().map((flight, i) => {
            return flight.routes.map((route, j) => {
              if (route.source && route.destination) {
                const source: Point = [route.source.longitude, route.source.latitude]
                const destination: Point = [route.destination.longitude, route.destination.latitude]

                return <Line id={`Line_${i}_${j}`} key={`Line_${i}_${j}`}
                  from={source}
                  to={destination}
                  strokeOpacity={0.9}
                  stroke={routeColors[i % 2]}
                  strokeWidth={4}
                  onMouseEnter={() => {
                    setTooltip(`${route.airline.airline}${route.flight_number} ${route.source_iata}-${route.destination_iata} ${Math.round(route.distance)}km ${formatMinutes(route.estimated_travel_time * 60)}`)
                  }}
                  onMouseLeave={() => {
                    setTooltip("")
                  }}
                // strokeLinecap="round"
                />
              }
              return null
            })
          })}




          {
            Object.values(airports).map((value: any, i) => {
              if (value.latitude && value.longitude) {
                return (<Marker key={i} coordinates={[value.longitude, value.latitude]}
                  onMouseEnter={() => {
                    setTooltip(`(${value.iata}) ${value.name}`)
                  }}
                  onMouseLeave={() => {
                    setTooltip("")
                  }}>
                  <circle r={4} stroke="black" fill="white" strokeWidth="2" />
                </Marker>)
              }
              return null
            })
          }
        </ZoomableGroup>
      </ComposableMap>
    </Paper >)
}

