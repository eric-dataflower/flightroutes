locals {
  workspaces = {
    "int": {
      # 496027045990 is the sandbox account.
      "account_id": "496027045990",

      # From `infra/vpc` output "vpc":
      "account_key": "sandbox"
    }
  }
  workspace = lookup(
    local.workspaces,
    terraform.workspace,
    local.workspaces["int"])
  account_id = local.workspace["account_id"]
  account_key = local.workspace["account_key"]
  vpc = data.terraform_remote_state.vpc.outputs.vpc[local.account_key]
  domain = (terraform.workspace == "prod" ? "dataflower.com.au"
    : "${terraform.workspace}.dataflower.com.au")
  server_domain_name = "flightroutes-api.${local.domain}"
  frontend_domain_name = "flightroutes-${local.domain}"
}

provider "aws" {
  region = "ap-southeast-2"
  assume_role {
    role_arn = "arn:aws:sts::${local.account_id}:role/Deploy"
  }
}

terraform {
  backend "s3" {
    bucket = "dataflower-terraform-state"
    key = "flightroutes.tfstate"
    region = "ap-southeast-2"
    encrypt = "true"
    dynamodb_table = "terraform-state-lock"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"
  workspace = "default"
  config = {
    region = "ap-southeast-2"
    bucket = "dataflower-terraform-state"
    key = "vpc.tfstate"
    encrypt = "true"
  }
}

data "terraform_remote_state" "ec2" {
  backend = "s3"
  workspace = "default"
  config = {
    region = "ap-southeast-2"
    bucket = "dataflower-terraform-state"
    key = "ec2.tfstate"
    encrypt = "true"
  }
}

data "terraform_remote_state" "ecs" {
  backend = "s3"
  workspace = "default"
  config = {
    region = "ap-southeast-2"
    bucket = "dataflower-terraform-state"
    key = "ecs.tfstate"
    encrypt = "true"
  }
}

data "terraform_remote_state" "acm" {
  backend = "s3"
  workspace = "default"
  config = {
    region = "ap-southeast-2"
    bucket = "dataflower-terraform-state"
    key = "acm.tfstate"
    encrypt = "true"
  }
}

resource "aws_security_group" "server" {
  vpc_id = local.vpc.aws_vpc.id
  name = "${terraform.workspace}-flightroutes-server"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Simply specify the family to find the latest ACTIVE revision in that family.
data "aws_ecs_task_definition" "server" {
  task_definition = "${aws_ecs_task_definition.server.family}"
  depends_on = [ aws_ecs_task_definition.server ]
}

resource "aws_cloudwatch_log_group" "server" {
  name = "/terraform/fargate/service/${terraform.workspace}-flightroutes-server"
  retention_in_days = 365
}


resource "aws_iam_role" "server" {
  name = "${terraform.workspace}-flightroutes-server"

  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
        "Effect": "Allow",
        "Principal": {
            "Service": "ecs-tasks.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
        }
    ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "server" {
  role       = aws_iam_role.server.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


resource "aws_ecs_task_definition" "server" {
  family = "${terraform.workspace}-flightroutes-server"
  cpu = 1024
  memory = 2048
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  execution_role_arn   = aws_iam_role.server.arn
  container_definitions = <<DEFINITION
[
  {
    "essential": true,
    "image": "registry.gitlab.com/eric-dataflower/flightroutes/server:latest",
    "name": "flightroutes-server",
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${aws_cloudwatch_log_group.server.name}",
        "awslogs-region": "ap-southeast-2",
        "awslogs-stream-prefix": "${terraform.workspace}-flightroutes-server"
      }
    },
    "environment": [
      {"name": "FRONTEND_DOMAIN_NAME", "value": "${local.frontend_domain_name}"}
    ]
  }
]
DEFINITION
}

resource "aws_lb_listener_rule" "server" {
  listener_arn = data.terraform_remote_state.ec2.outputs.ec2[local.account_key].aws_lb_listener.arn
  priority = 100

  action {
    type = "forward"
    target_group_arn = aws_lb_target_group.server.arn
  }

  condition {
    host_header {
      values = [local.server_domain_name]
    }
  }
}

resource "aws_lb_target_group" "server" {
  name = "${terraform.workspace}-flightroutes-server"
  port = 80
  target_type = "ip"
  protocol = "HTTP"
  vpc_id = local.vpc.aws_vpc.id
}

resource "aws_ecs_service" "server" {
  name = "${terraform.workspace}-flightroutes-server"
  cluster = data.terraform_remote_state.ecs.outputs.ecs_cluster[local.account_key].id
  task_definition = "${aws_ecs_task_definition.server.family}:${max("${aws_ecs_task_definition.server.revision}", "${data.aws_ecs_task_definition.server.revision}")}"
  launch_type = "FARGATE"
  desired_count = 2

  network_configuration {
    subnets = local.vpc.public_subnets
    security_groups = [
      aws_security_group.server.id
    ]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.server.arn
    container_name = "flightroutes-server"
    container_port = 80
  }

  lifecycle {
    ignore_changes = [desired_count]
  }
}


resource "aws_s3_bucket" "frontend" {
  bucket = local.frontend_domain_name
  acl = "public-read"
  force_destroy = true

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT","POST"]
    allowed_origins = ["*"]
    expose_headers = ["ETag"]
    max_age_seconds = 3000
  }

  policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "PublicReadForGetBucketObjects",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${local.frontend_domain_name}/*"
    },
    {
      "Sid": "PublicReadGetObject",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${local.frontend_domain_name}/*",
      "Condition": {
        "IpAddress": {
          "aws:SourceIp": [
              "2400:cb00::/32",
              "2405:8100::/32",
              "2405:b500::/32",
              "2606:4700::/32",
              "2803:f800::/32",
              "2c0f:f248::/32",
              "2a06:98c0::/29",
              "103.21.244.0/22",
              "103.22.200.0/22",
              "103.31.4.0/22",
              "104.16.0.0/12",
              "108.162.192.0/18",
              "131.0.72.0/22",
              "141.101.64.0/18",
              "162.158.0.0/15",
              "172.64.0.0/13",
              "173.245.48.0/20",
              "188.114.96.0/20",
              "190.93.240.0/20",
              "197.234.240.0/22",
              "198.41.128.0/17"
          ]
        }
      }
    }
  ]
}
EOF
  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

resource "aws_s3_bucket_object" "frontend" {
  for_each = fileset(path.module, "src/frontend/build/**/*")

  bucket = aws_s3_bucket.frontend.bucket
  key = trimprefix(each.value, "src/frontend/build/")
  content_type = lookup({
    ".txt" = "text/plain; charset=utf-8"
    ".html" = "text/html; charset=utf-8"
    ".png" = "image/png"
    ".json" = "application/json"
    ".js" = "application/javascript"
    ".css" = "text/css"
    ".svg" = "image/svg+xml"
    ".map" = "application/octet-stream"
  }, regex("\\.[^.]+$", each.value), "binary/octet-stream")
  source = "${path.module}/${each.value}"
  etag = filemd5(each.value)
}
